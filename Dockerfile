FROM  python:3
WORKDIR /.
COPY . .
CMD ["main.py"]
ENTRYPOINT ["python3"]
